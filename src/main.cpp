#include <Arduino.h>
#include <FastLED.h>
#include <ClickEncoder.h>
#include <Ticker.h>
#include <EEPROMex.h>

#include <avr/power.h>

//#define LED_PIN 12
//#define LED_NUM 12
#define LED_NUM 28
#define FRAMES_PER_SECONDS 100
#define BRIGHTNESS 255

#define EEPROM_ADDR_MODE 1
#define EEPROM_ADDR_BRIGHTNES 2
#define EEPROM_ADDR_SPEED 3

#define STEPS 4
//#define PIN_A0 27
//#define PIN_A1 14
//#define PIN_SW 26

#define LED_PIN 8
#define PIN_A0 17
#define PIN_A1 18
#define PIN_SW 19

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

enum Mode {
	Effect,
	Bright
};

void rainbow();
void rainbowWithGlitter();
void fill();
void rainbowRound();
void confetti();
void sinelon();
void juggle();
void bpm();
void nextPattern();



CRGBArray<LED_NUM> leds;
ClickEncoder encoder(PIN_A0, PIN_A1, PIN_SW, STEPS);

typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { rainbowRound, rainbow, fill, rainbowWithGlitter, confetti, sinelon, juggle, bpm };

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

int8_t gSpeed = 0;
int16_t gPartSpeed = 0;
uint8_t gBrightness = BRIGHTNESS;

uint8_t gMode = 0;
bool gHeld = false;

void cbTickerEncoder() {
	encoder.service();
}
Ticker tickerEncoder(cbTickerEncoder, 1);

void cbLed() {
	gPartSpeed += gSpeed;
	if(gPartSpeed>100) {
		++gHue;
		gPartSpeed = 0;
	}
	gPatterns[gCurrentPatternNumber]();
	FastLED.show();
}
Ticker tickerLed(cbLed, 1000/FRAMES_PER_SECONDS);

void setup() {
	clock_prescale_set(clock_div_1);

	FastLED.addLeds<WS2812B,LED_PIN, EOrder::GRB>(leds, LED_NUM).setCorrection(TypicalLEDStrip);
	FastLED.setBrightness(gBrightness);

	encoder.setAccelerationEnabled(true);

	leds.fill_solid(CRGB::Black);
	FastLED.show();

	gCurrentPatternNumber = EEPROM.readByte(EEPROM_ADDR_MODE) % ARRAY_SIZE(gPatterns);
	gBrightness = EEPROM.readByte(EEPROM_ADDR_BRIGHTNES);
	gSpeed = EEPROM.readByte(EEPROM_ADDR_SPEED);

	tickerEncoder.start();
	tickerLed.start();
}

void loop() {
	tickerEncoder.update();
	tickerLed.update();

	int value = encoder.getValue();
	switch(gMode) {
		case Effect:
			gSpeed += value;
			if(gSpeed < 0) {
				gSpeed = 0;
			} else if(gSpeed > 100) {
				gSpeed = 100;
			}
			break;
		case Bright:
			gBrightness += value;
			FastLED.setBrightness(gBrightness);
			break;
	}

	uint8_t buttonState = encoder.getButton();
	if(buttonState!=0) {
		switch(buttonState) {
			case ClickEncoder::Clicked:
				nextPattern();
				break;
			case ClickEncoder::DoubleClicked:
				if(gMode==Effect) {
					gMode = Bright;
				} else {
					gMode = Effect;
				}
				break;
			case ClickEncoder::Held:
				gHeld = true;
				break;
			case ClickEncoder::Released:
				if(gHeld == true) {
					gHeld = false;

					leds.fill_solid(CRGB::Black);
					FastLED.show();
					FastLED.delay(100);
					EEPROM.writeByte(EEPROM_ADDR_MODE, gCurrentPatternNumber);

					leds.fill_solid(CRGB::White);
					FastLED.show();
					FastLED.delay(100);
					EEPROM.writeByte(EEPROM_ADDR_BRIGHTNES, gBrightness);

					leds.fill_solid(CRGB::Black);
					FastLED.show();
					FastLED.delay(100);
					EEPROM.writeByte(EEPROM_ADDR_SPEED, gSpeed);

					leds.fill_solid(CRGB::White);
					FastLED.show();
					FastLED.delay(100);

					for(uint8_t a=0; a<3; ++a) {
						leds.fill_solid(CRGB::Black);
						FastLED.show();
						FastLED.delay(100);

						leds.fill_solid(CRGB::White);
						FastLED.show();
						FastLED.delay(100);
					}
				}
		}
	}
}

void nextPattern()
{
	// add one to the current pattern number, and wrap around at the end
	gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE(gPatterns);
}

void rainbow()
{
	// FastLED's built-in rainbow generator
	fill_rainbow( leds, LED_NUM, gHue, 5);
}

void rainbowRound()
{
	fill_rainbow( leds, LED_NUM, gHue, 256/LED_NUM);
}

void addGlitter( fract8 chanceOfGlitter)
{
	if( random8() < chanceOfGlitter) {
		leds[ random16(LED_NUM) ] += CRGB::White;
	}
}

void fill()
{
	leds.fill_solid(CHSV(gHue, 255, 255));
}

void rainbowWithGlitter()
{
	// built-in FastLED rainbow, plus some random sparkly glitter
	rainbow();
	addGlitter(80);
}

void confetti()
{
	// random colored speckles that blink in and fade smoothly
	fadeToBlackBy( leds, LED_NUM, 10);
	int pos = random16(LED_NUM);
	leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
	// a colored dot sweeping back and forth, with fading trails
	fadeToBlackBy( leds, LED_NUM, 20);
	int pos = beatsin16( 13, 0, LED_NUM-1 );
	leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
	// colored stripes pulsing at a defined Beats-Per-Minute (BPM)
	uint8_t BeatsPerMinute = 62;
	CRGBPalette16 palette = PartyColors_p;
	uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
	for( int i = 0; i < LED_NUM; i++) { //9948
		leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
	}
}

void juggle() {
	// eight colored dots, weaving in and out of sync with each other
	fadeToBlackBy( leds, LED_NUM, 20);
	byte dothue = 0;
	for( int i = 0; i < 8; i++) {
		leds[beatsin16( i+7, 0, LED_NUM-1 )] |= CHSV(dothue, 200, 255);
		dothue += 32;
	}
}
